<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


//*******************************API RESTFUL ********************/
//add user
Route::post('/user', 'API\UserController@addUser');

//delete user
Route::post('/user/{id}', 'UserController@deleteUser');

//update user
Route::put('/user/{id}', 'UserController@updateUser');


Route::middleware('auth:api')->group(function () {
    return Route::resource('User','API\UserController');
});


