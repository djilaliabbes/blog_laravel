<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/profil',function(){
    return view('profil');
});
//afficher tous les posts
Route::get('/my-posts/{user_id}', 'postController@allPosts')->where('user_id','[0-9]+')->name("my-all-posts");

//afficher tous les posts
Route::get('/posts', 'postController@allPosts')->name("all-posts");

//Posts archived routes
Route::get('/posts/archived', 'postController@allPostsArchived')->name("all-posts-archived");
Route::post('/restore-post/{id}', 'postController@restorePost')->name("restore-post");
Route::post('/destroy-post/{id}', 'postController@destroyPost')->name("destroy-post");





//afficher le détail d'un post
Route::get('/show-post/{id}','postController@showPost')->name('show-post');

//Afficher le formulaire d'ajout ou de modification d'un post 
Route::get('/form-post','postController@showForm')->name('form-post-get');

//Récupérer les informations d'un post en ajout
Route::post('/add-post','postController@addPost');
//Récupérer les informations d'un comment 
Route::post('/add-comment','postController@addComment');

//Récupérer les informations d'un post en modification
Route::put('/update-post','postController@updatePost');
//Supprimer un post 
Route::delete('/delete-post/{id}','postController@deletePost');



Auth::routes();

Route::get('/profil', 'HomeController@index')->name('profil');
Route::post('/update-profil', 'HomeController@updateProfil');
Route::get('/logout', 'HomeController@logout');

