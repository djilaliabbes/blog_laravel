<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        $user= $request->user();
        $comments = $user->comments;

        return view('profil',['user'=>$user,'comments'=>$comments]);
    }
    public function updateProfil(Request $request)
    {
       

        User::updateProfil($request);
        return back();
    }

    public function logout(){
        Auth::logout();
        return redirect()->route('all-posts');
    }


 









}
