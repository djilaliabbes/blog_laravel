<?php

namespace App\Http\Controllers;

use App\Comment;
use Illuminate\Http\Request;
use \App\Post;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class postController extends Controller
{
    public function __construct()
    {
        $this->middleware("isConnected")->except(['allPosts', 'showPost']);
    }


    //Afficher tous les posts 
    public function allPosts($user_id = null)
    {
        if($user_id){
            $posts = Post::where('user_id',$user_id)->get();
        }else{
            //Find all posts 
            $posts = Post::all();
        }
        return view('all-posts',[
            'posts'=>$posts,
            'msg'=>false 
            ]);

    }
    //afficher les articles archivé
    public function allPostsArchived(){
        //Find all posts 
        $postsArchived = Post::onlyTrashed()->where('user_id',"=",Auth::user()->id)->get();
        return view('all-posts',[
            'posts'=>$postsArchived,
            'msg'=>false 
            ]);
    }
    //Afficher le détail d'un post
    public function showPost($id){
        //dd($id);
        //si pas d'id ou que l'id de post n'existe pas je redirige vers la page des articles
        if(!$id){
            return redirect()->route('"all-posts"');
        }
        //Trouver l'article 
            $post = Post::find($id);
            $comments = $post->comments->sortByDesc('created_at');;
        //dd($comments);
        //Return details view
        return view('detail-post',[
            'post'=>$post ? $post : false,
            'comments'=>$comments ? $comments : false
        ]);
    }
    //afficher le formulaire d'ajout ou de modification d'article
    public function showForm(Request $request){
        $post = false;
        //Si j'ai un id -->trouver l'article
        if($request && $request->id){
            $post = Post::find($request->id);
        }
        return view('form-post',[
            'error'=>false,
            'post'=>$post,
        ]);
    }

    //ajouter un post 
    public function addPost(Request $request)
    { //si j'ai un id --> je suis en modification 
       
        if(!$request->title || !$request->content || !$request->author){
            return redirect()->route('form-post-get', ['error' => true]);
        }elseif(!$request->id){
            $id = Post::createPost($request);
            if($id) return redirect()->route('show-post',['id'=>$id]);
        }else{
            //Requête avec eloquent
            $post = Post::find($request->id);
            $post->title = $request->title;
            $post->content = $request->content;
            $post->author = $request->author;
            $post->updated_at = now();
            $post->save();

            //Requête avec query builder
            // DB::table('posts')
            // ->where('id',$request->id)
            // ->update([
            //     'title'=>$request->title,
            //     'content'=> $request->content,
            //     'author'=>$request->author,
            //     'updated_at'=>now()
            //  ]); 
             return redirect()->route('show-post',['id'=>$request->id]);      
        }
        return redirect()->route('form-post-get', ['error' => true]);
    }
    //Supprimer un post
    public function deletePost(Request $request)
    {

        if($request->id){
            //archiver l'article --> deleted_at
            $post = Post::find($request->id);
            $post->delete();

        return json_encode(['msg'=>"L'article à bien été supprimé"]);  
        }
    }
    //destroy un post 
    public function destroyPost(Request $request)
    {

        if($request->id){
            //archiver l'article --> deleted_at
            $post = Post::withTrashed()
                        ->where('id',$request->id)
                        ->forceDelete();

        return json_encode(['msg'=>"L'article à bien été supprimé"]);  
        }
    }
    
    //restore un post 
    public function restorePost(Request $request)
    {
        if($request->id){
            //restaurer l'article
            Post::withTrashed()
                        ->where('id', $request->id)
                        ->restore();
        return json_encode(['msg'=>"L'article à bien été restauré"]);  
        }
    }

    public function addComment(Request $request)
    {
        Comment::addComment($request);
        return back();
    }
    
      

}
