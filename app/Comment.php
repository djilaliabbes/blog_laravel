<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Comment extends Model
{
     //Nom de ma table 
     protected $table = 'comments';

     //les champs de ma table obligatoire
     protected $fillable = ['content','postId','userId'];

    static function addComment($request)
    {
       if($request){
           // Si l'article existe ok sinon pas de comment
       }
       $comment = new Comment();
       $comment->content = $request->content;
       $comment->user_id = Auth::user()->id;
       $comment->post_id = $request->postId;
       $comment->created_at = now();
       $comment->save();
       return $comment->id;

    }
    public function post()
    {
        return $this->belongsTo('App\Post','post_id');
    }
    public function user()
    {
        return $this->belongsTo('App\User','user_id');
    }
}
