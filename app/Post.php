<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Post extends Model
{
    //Nom de ma table 
    protected $table = 'posts';

    //les champs de ma table obligatoire
    protected $fillable = ['title','content','author'];

    //soft deleting
    use SoftDeletes;
    static function createPost($request){
        $post = new Post;
        $post->title = $request->title;
        $post->content = $request->content;
        $post->author = $request->author;
        $post->created_at = now();
        $post->user_id = $request->userId;
        $post->save();
        return $post->id;
    }
        /**
     * Get the comments for post.
     */
    public function comments()
    {
        return $this->hasMany('App\Comment','post_id');
    }

}
