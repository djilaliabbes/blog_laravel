<?php

namespace App;


use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable ;
use Illuminate\Support\Facades\Storage;

class User extends Authenticatable
{
     //Nom de ma table 
     protected $table = 'users';

     //les champs de ma table obligatoire
     protected $fillable = ['name','email','password'];
 
     //soft deleting
     use SoftDeletes;
     // public function comments()
     // {
     //     return $this->hasMany('App\Comment','user_id');
     // }
     public function comments()
     {
         return $this->hasManyThrough('App\Comment', 'App\Post');
     }
     static function updateProfil($request){
         if(is_file($request->file('avatar'))){
            $content = $request->file('avatar');
            $newName = md5_file($content->getRealPath());
            $guessFileExtension = $content->guessExtension();
            $nameFile = $newName.'.'.$guessFileExtension;
            //dd($file);
            Storage::putFileAs('images', $request->file('avatar'), $nameFile,'public');
         }
        
          $user = $request->user();
          $user->name = $request->name;
          $user->email = $request->email;
          $user->updated_at = now();
          if($nameFile) $user->avatar = $nameFile;
         //avatar 
         $user->save();
     }
 
 
}
