<?php
use Illuminate\Database\Seeder;
use App\User;
use App\Post;

class PostsSeeder extends Seeder {
    public function run()
    {
        $faker = Faker\Factory::create('fr_FR');
        for ($i = 0; $i < 10; $i++) {
            $post = App\Post::create([
                'title' => $faker->title,
                'content'=> $faker->text,
                'author'=>$faker->lastName
            ]);
        }
    }
}