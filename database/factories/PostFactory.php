<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Post;
use App\User;
use Faker\Generator as Faker;

$factory->define(Post::class, function (Faker $faker) {
    $faker = Faker\Factory::create('fr_FR');
    return [
        'titre' => $faker->title,
        'contenu' => $faker->paragraph,
        'author' => $faker->lastName,
    ];
});
