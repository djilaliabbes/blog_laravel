
window.showPost = function (id,event){
    event.stopPropagation();
    location = `/show-post/${id}`;
};
window.deletePost =  function(id,event){
    event.stopPropagation();
    console.log(id);
    //location = `/delete-post`;
    let url = `/delete-post/${id}`;
   fetch(url,{
         headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
         },
        method: 'delete',
    })
    .then(response=>{
        if(response.ok){
            return response.json();
        }
    })
    .then(res=>{
        console.log(res);
        if(res.msg){
            location = `/posts`;
        }
    })
    .catch(error=>console.log(error));
};
window.restorePost= function(id,event){
    event.stopPropagation();
    let url = `/restore-post/${id}`;
    fetch(url,{
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token-restore"]').attr('content')
         },
        method : "post"
    })
    .then(response=>{
        if(response.ok) return response.json();
    })
    .then(result=>{
        console.log(result);
        if(result && result.msg){
            document.querySelector(`#js-post${id}`).style.display = "none";
            const msg = document.querySelector("#msg");
            msg.innerHTML=`<div class="alert alert-success" role="alert"> Article restauré </div>`;    
        }
    })
    .catch(error => console.log(error));
}
window.destroyPost = function(id,event){
    event.stopPropagation();
    console.log(id);
    let url = `/destroy-post/${id}`;
    fetch(url,{
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token-destroy"]').attr('content')
         },
        method : "post"
    })
    .then(response=>{
        if(response.ok) return response.json();
    })
    .then(result=>{
        console.log(result);
        if(result && result.msg){
            document.querySelector(`#js-post${id}`).style.display = "none";
            const msg = document.querySelector("#msg");
            msg.innerHTML=`<div class="alert alert-success" role="alert"> Article supprimé</div>`;    
        }
    })
    .catch(error => console.log(error));

}