@extends('layouts.mainlayout')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Bienvenu sur le blog
                      <!-- Button trigger modal -->
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modalUpdateProfil">
                    modifier mes infos
                </button>
                </div>
                <div class="card-body">
                    @if ($user)
                        <div class="alert alert-success" role="alert">
                            {{ ($user->name) }}
                        </div>
                    @endif
                </div>
            </div>
        </div>
        <div class="col-md-6">
        <img  src= "{{URL::asset('/storage/images/'. $user->avatar)}}">
        </div>
        <div class="col-md-2">
            <h1>Liste des commentaires </h1>
            @foreach ($comments as $comment)
            <div class="card mb-2">
                <div class="card-header">{{$comment->user->name }}</div>
                <div class="card-body">
                    <blockquote class="blockquote mb-0">
                        <p>{{ $comment->content  }}</p>
                        <footer class="blockquote-footer">{{$comment->created_at }}</footer>
                    </blockquote>
                </div>
            </div>
        @endforeach
        </div>

    </div>
  
  <!-- Modal -->
  <div class="modal fade" id="modalUpdateProfil" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLongTitle">Mes infos</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form action="/update-profil" method="post" enctype='multipart/form-data'>
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <div class="modal-body">


                {{-- titre de l'article --}}
                <div class="form-group">
                  <input type="text" class="form-control"
                  value="{{$user->name}}"
                   name="name" id="name" >
                </div>
          
                {{-- Contenu de l'article --}}
                <div class="form-group">
                  <input class="form-control" name="email"
                   id="email" 
                    value="{{$user->email}}">
                </div>
                <div class="form-group">
                    <label>Télecharger une photo </label>
                      <input class="form-controle"  type="file" name="avatar">
                  </div>
                  
       
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary mb-2">Modifier</button>
        </div>
    </form>
      </div>
    </div>
  </div>
</div>
@endsection
