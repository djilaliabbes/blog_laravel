@extends('layouts.mainlayout')
@section('content')
@if($error)
    <div class="alert alert-danger" role="alert">
          Vous devez renseigner tous les champs s'il vous plaît
     </div>
@endif
<div class="container">
    <form action="/add-post" method="post" id="form-add-post">
      <input type="hidden" name="_token" value="{{ csrf_token() }}">
      <input type="hidden" name="id" value="{{ $post ? $post->id : '' }}">


      {{-- titre de l'article --}}
      <div class="form-group">
        <input type="text" class="form-control"
        value="{{$post ? $post->title : ''}}"
         name="title" id="title" placeholder="Titre de votre article">
      </div>

      {{-- Contenu de l'article --}}
      <div class="form-group">
        <textarea class="form-control" name="content"
         id="content" 
         placeholder="Contenu de votre article"
          rows="10" >{{$post ? $post->content :''}}</textarea>
      </div>

      {{--  --}}
      <div class="form-group">
          <input type="hidden" name="author" id="author"
          value="{{Auth::user()->name}}">
          <input type="hidden" name="userId" id="userId"
          value="{{Auth::user()->id}}">
      </div>
      <button type="submit" class="btn btn-primary mb-2">Publier</button>
    </form>
</div>

@endsection
