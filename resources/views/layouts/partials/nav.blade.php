<nav class="navbar navbar-expand-lg navbar-light bg-light style="background-color: #e3f2fd;">
    <a class="navbar-brand" href="/home">Blog Laravel</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
  
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav mr-auto">
        <li class="nav-item active">
          <a class="nav-link" href="/home">Accueil</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="/posts">Articles</a>
        </li>
        @guest
        <li class="nav-item">
          <a class="nav-link" href="/login">Se connecter</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="/register">Créer un compte</a>
        </li>
        @else
          <li class="nav-item">
            <a class="nav-link" href="/form-post">Créer un article</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="/posts/archived">Archive</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="/my-posts/{{Auth::user()->id}}">Mes articles</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="/profil">Profil</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="/logout">Se déconnecter</a>
          </li>
        @endguest

      </ul>
      <form class="form-inline my-2 my-lg-0">
        <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
        <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
      </form>
    </div>
  </nav>