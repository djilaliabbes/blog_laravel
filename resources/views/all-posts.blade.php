@extends('layouts.mainlayout')
@section('content')
    <div class="container">
        @if (!$posts)
            <p>Aucun article, Vous pouvez en créer un <a href="/form-post">par là</a></p>
        @endif
        @if ($msg)
    <div>{{$msg}}</div>
            
        @endif
        <h1>Tous les posts</h1>
        <div id="msg"></div>
         @foreach($posts as $post)
            <div class="jumbotron" id="js-post{{$post->id}}"
                 @if ($post->deleted_at)
                    ''
                @else
                    onclick="showPost({{$post->id}},event)"
                @endif >
                
            @guest
                
            @else
                @if ($post->deleted_at)
                    <a href="/form-post?id={{$post->id}}">Modifier l'article</a>
                    <meta name="csrf-token-restore" content="{{ csrf_token() }}">
                    <button   onclick="restorePost({{$post->id}},event)">Restaurer l'article</button>
                    <meta name="csrf-token-destroy" content="{{ csrf_token() }}">
                    <button   onclick="destroyPost({{$post->id}},event)">supprimer l'article définitivement</button>
                @else
                @if (Auth::user()->id == $post->user_id)
                    <a href="/form-post?id={{$post->id}}">Modifier l'article</a>
                    <meta name="csrf-token" content="{{ csrf_token() }}">
                    <button  id="js-delete-post" onclick="deletePost({{$post->id}},event)">supprimer l'article</button>
                @endif
                   
                @endif
   
            @endguest
               
            <h3>{{ $post->title }}</h3>
            <p>{{ $post->content }}</p>
            
            <!-- lien vers le profil de l'auteur  -->
            <a href="#">{{ $post->author }}</a>
             </div>
        @endforeach
      
    </div>

     
@endsection
