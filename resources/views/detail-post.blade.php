@extends('layouts.mainlayout')
@section('content')
   @if (!$post)
      <p>Pas d'article avec ses informations <a href="/posts">Revenir vers la liste des articles</a></p> 
    @else
   
    <div class="jumbotron container mt-3">
    <a href="/form-post?id={{$post->id}}">Modifier l'article</a>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <button id="js-delete-post" onclick="deletePost({{$post->id}},event)">supprimer l'article</button>
        <h1>{{ $post->title }}</h1>
        <p>
            {{ $post->content }}
        </p>
        <!-- lien vers le profil de l'auteur  -->
        <a href="#">{{ $post->author }}  {{$post->updated_at ? $post->updated_at : $post->created_at}}</a>
        <form action="/add-comment" method="post" >
            <div class="form-group">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <input type="text" name="content" id="content" placeholder="Commenter l'article">
                <button type="submit">Commenter</button>
                <input type="hidden" name="postId" id="postId"
                value="{{$post->id}}">
            </div>
        </form>
        @foreach ($comments as $comment)
            <div class="card mb-2">
                <div class="card-header">{{$comment->user->name }}</div>
                <div class="card-body">
                    <blockquote class="blockquote mb-0">
                        <p>{{ $comment->content  }}</p>
                        <footer class="blockquote-footer">{{$comment->created_at }}</footer>
                    </blockquote>
                </div>
            </div>
        @endforeach
           
    </div>


   @endif
@endsection